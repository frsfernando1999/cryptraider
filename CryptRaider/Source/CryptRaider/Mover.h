// Copyright Epic Games, Inc. All Rights Reserved.

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Mover.generated.h"

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class CRYPTRAIDER_API UMover : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UMover();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
	void SetShouldMove(bool TurnOn);

private:
	UPROPERTY(EditAnywhere, Category="Move Object")
	FVector MoveOffset;

	UPROPERTY(EditAnywhere, Category="Move Object")
	float MoveTime = 4;

	UPROPERTY(EditAnywhere, Category="Move Object") 
	bool ShouldMove = false;

	FVector OriginalLocation;
};
