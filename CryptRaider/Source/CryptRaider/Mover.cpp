// Copyright Epic Games, Inc. All Rights Reserved.

#include "Mover.h"
#include "Math/UnrealMathUtility.h"

// Sets default values for this component's properties
UMover::UMover()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UMover::BeginPlay()
{
	Super::BeginPlay();
	OriginalLocation = GetOwner()->GetActorLocation();


	// ...
}

// Called every frame
void UMover::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	FVector CurrentLocation = GetOwner()->GetActorLocation();
	if (ShouldMove)
	{
		FVector TargetLocation = OriginalLocation + MoveOffset;
		float Speed = FVector::Distance(OriginalLocation, TargetLocation) / MoveTime;
		FVector NewLocation = FMath::VInterpConstantTo(CurrentLocation, TargetLocation, DeltaTime, Speed);
		GetOwner()->SetActorLocation(NewLocation);
	}
	else
	{
		float Speed = MoveOffset.Length() / MoveTime;
		FVector NewLocation = FMath::VInterpConstantTo(CurrentLocation, OriginalLocation, DeltaTime, Speed);
		GetOwner()->SetActorLocation(NewLocation);
	}

	/* 	AActor *Owner = GetOwner();
		FVector Location = Owner->GetActorLocation();

		UE_LOG(LogTemp, Display, TEXT("Owner: %s | Address: %u | Location: %s"), *(Owner->GetActorNameOrLabel()), Owner, *(Location.ToCompactString()));
	 */
}

void UMover::SetShouldMove(bool TurnOn)
{
	ShouldMove = TurnOn;
}
