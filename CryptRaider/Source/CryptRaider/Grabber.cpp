// Fill out your copyright notice in the Description page of Project Settings.

#include "Grabber.h"

#include "Engine/World.h"
#include "DrawDebugHelpers.h"

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();
	// ...
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UPhysicsHandleComponent *PhysicsHandle = GetHandleComponent();
	if (PhysicsHandle == nullptr || PhysicsHandle->GetGrabbedComponent() == nullptr)
	{
		return;
	}
		FVector TargetLocation = GetComponentLocation() + GetForwardVector() * HoldDistance;
		PhysicsHandle->SetTargetLocationAndRotation(TargetLocation, GetComponentRotation());
	
	/* 	float Damage;
		float &DamageRef = Damage;
		if (HasDamage(Damage))
		{
			PrintDamage(Damage);
		}
	 */
	// ...
}

void UGrabber::Release()
{
	UPhysicsHandleComponent *PhysicsHandle = GetHandleComponent();
	if (PhysicsHandle == nullptr || PhysicsHandle->GetGrabbedComponent() == nullptr)
	{
		return;
	}
	UPrimitiveComponent *GrabbedComponent = PhysicsHandle->GetGrabbedComponent();
	GrabbedComponent->WakeAllRigidBodies();
	GrabbedComponent->GetOwner()->Tags.Remove("Grabbed");
	PhysicsHandle->ReleaseComponent();
	// UE_LOG(LogTemp, Display, TEXT("Released"));
}

void UGrabber::Grab()
{
	UPhysicsHandleComponent *PhysicsHandle = GetHandleComponent();
	if (PhysicsHandle == nullptr)
	{
		return;
	}

	FHitResult HitResult;
	bool HasHit = GetGrabbableInReach(HitResult); 

	if (HasHit)
	{
		AActor* Actor = HitResult.GetActor();
		HitResult.GetComponent()->WakeAllRigidBodies();
		Actor->Tags.Add("Grabbed");
		Cast<UPrimitiveComponent>(Actor->GetRootComponent())->SetSimulatePhysics(true);
		Actor->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		PhysicsHandle->GrabComponentAtLocationWithRotation(
			HitResult.GetComponent(),
			NAME_None,
			HitResult.ImpactPoint,
			GetComponentRotation());
		// DrawDebugSphere(GetWorld(), HitResult.ImpactPoint, 10, 10, FColor::Blue, false, 5);
	}
}

UPhysicsHandleComponent *UGrabber::GetHandleComponent() const
{
	UPhysicsHandleComponent *Result = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (Result == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("This game requires a UPhysicsHandleComponent"));
	}
	return Result;
}

bool UGrabber::GetGrabbableInReach(FHitResult& OutHitResult) const
{
	// DrawDebugLine(GetWorld(), Start, End, FColor::Red, false, 5);
	// DrawDebugSphere(GetWorld(), End, 10, 10, FColor::Blue, false, 5);

	FVector Start = GetComponentLocation();
	FVector End = Start + GetForwardVector() * MaxGrabDistance;
	FCollisionShape Sphere = FCollisionShape::MakeSphere(GrabRadius);
		return GetWorld()->SweepSingleByChannel(
		OutHitResult,
		Start,
		End,
		FQuat::Identity,
		ECC_GameTraceChannel2,
		Sphere);
}

/* void UGrabber::PrintDamage(const float &Damage)
{
	UE_LOG(LogTemp, Display, TEXT("Damage: %f"), Damage);
}

bool UGrabber::HasDamage(float &OutDamage)
{
	OutDamage = 5;
	return true;
}
 */